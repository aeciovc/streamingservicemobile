package br.org.cesar.streamingservicemobile.anymote;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.widget.ProgressBar;

import com.example.google.tv.anymotelibrary.client.AnymoteClientService.ClientListener;
import com.example.google.tv.anymotelibrary.client.AnymoteClientService;
import com.example.google.tv.anymotelibrary.client.AnymoteSender;

public class AnymoteCommunication implements ClientListener {

    /**
     * This manages discovering, pairing and connecting to Google TV devices on
     * network.
     */
	
	private static AnymoteCommunication mInstance;
	
	private AnymoteClientService mAnymoteClientService;
    private ProgressBar progressBar;
    private static Context mContext;
    
    private AnymoteSender mAnymoteSender;
    
    private ConnectionStatus mConnectionStatus;
    private OnAnymoteConnectionListener mOnAnymoteConnectionListener;

    public interface OnAnymoteConnectionListener{
    	public void onConnection(ConnectionStatus mConnectionStatus);
    }
    
    public static AnymoteCommunication getInstance(Context context){
    	if (mInstance == null){
    		mInstance = new AnymoteCommunication(context);
    	}
    	
    	return mInstance;
    }
    
    private AnymoteCommunication(Context context){
    	mContext = context;
    }
	
    public void startCommunication(OnAnymoteConnectionListener onAnymoteConnectionListener){
    	// Bind to the AnymoteClientService
        Intent intent = new Intent(mContext, AnymoteClientService.class);
        mContext.bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        
        mOnAnymoteConnectionListener = onAnymoteConnectionListener;
    }
    
    public boolean sendIntent(Intent intent){
    	
    	if (mConnectionStatus == ConnectionStatus.CONNECTED){

            mAnymoteSender.sendIntent(intent);
            
            return true;
    	}
    	
    	return false;
    }

    public boolean sendData(String value){
    	
    	if (mConnectionStatus == ConnectionStatus.CONNECTED){
            
    		mAnymoteSender.sendData("teste");
    		
    		//mAnymoteSender.sendData(data)
    		
    		return true;
    	}
    	
    	return false;
    }

    
	@Override
	public void onConnected(AnymoteSender anymoteSender) {
		
		mConnectionStatus = ConnectionStatus.CONNECTED;
		
		this.mAnymoteSender = anymoteSender;
		
		mOnAnymoteConnectionListener.onConnection(mConnectionStatus);
	}

	@Override
	public void onDisconnected() {
		mConnectionStatus = ConnectionStatus.DISCONNECTED;
		
		mOnAnymoteConnectionListener.onConnection(mConnectionStatus);
		
		mContext.unbindService(mConnection);
	}

	@Override
	public void onConnectionFailed() {
		mConnectionStatus = ConnectionStatus.DISCONNECTED;
		
		mOnAnymoteConnectionListener.onConnection(mConnectionStatus);
	}
	
	   /** Defines callbacks for service binding, passed to bindService() */
    private ServiceConnection mConnection = new ServiceConnection() {
        /*
         * ServiceConnection listener methods.
         */
        public void onServiceConnected(ComponentName name, IBinder service) {
            mAnymoteClientService = ((AnymoteClientService.AnymoteClientServiceBinder) service)
                    .getService();
            mAnymoteClientService.attachClientListener(AnymoteCommunication.this);
        }

        public void onServiceDisconnected(ComponentName name) {
            mAnymoteClientService.detachClientListener(AnymoteCommunication.this);
            mAnymoteClientService = null;
        }
    };

}
