package br.org.cesar.streamingservicemobile.anymote;

public enum ConnectionStatus {
	
	CONNECTED, DISCONNECTED

}
