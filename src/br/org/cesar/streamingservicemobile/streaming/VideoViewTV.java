package br.org.cesar.streamingservicemobile.streaming;

import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.MediaController;
import android.widget.VideoView;
import br.org.cesar.streamingservicemobile.model.Video;
import br.org.cesar.streamingservicemobile.util.Util;

/*Links Testes
http://172.28.145.111:8080/teste
/storage/sdcard0/DCIM/Camera/20130422_172901.mp4
/Root/sdcard/DCIM/Natiruts.mp4
http://daily3gp.com/vids/747.3gp
http://crk_br_am_ios-i.akamaihd.net/i/1/w/z6/jsvlf_H264_IOS_,100BR,200BR,350BR,500BR,650BR,800BR,900BR,.mp4.csmil/master.m3u8?__b__=400&__a__=off //Crackle funcionando
rtsp://172.28.146.198/sample-sd.3gp //Evandro Server QuickTime
rtsp://v6.cache2.c.youtube.com/CjYLENy73wIaLQkZwGsSdpQfpRMYDSANFEIJbXYtZ29vZ2xlSARSBXdhdGNoYOfp3IHRr4fOUAw=/0/0/0/video.3gp
*/	

public class VideoViewTV extends VideoView {

	private Context mContext;
	
	private int mVideoWidth;
	private int mVideoHeight;
    
    private ProgressDialog progressDialog;
	
	public VideoViewTV(Context context) {
		super(context);
		mContext = context;
		initVideoView();
	}
	
	public VideoViewTV(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
		initVideoView();
	}
	
	private void initVideoView(){
		//Para qualquer audio que estiver tocando
		Intent i = new Intent("com.android.music.musicservicecommand");
		i.putExtra("command", "pause");
		getContext().sendBroadcast(i);
		
		progressDialog = new ProgressDialog(mContext);
		progressDialog.setMessage("Loading...");
		progressDialog.show();
	}
	
	public void setVideo(Video video) {
		setMediaController(mMediaController);
		mMediaController.setAnchorView(this);
				
		setVideoURI(Uri.parse(video.getAddress()));
		requestFocus();
		setOnPreparedListener(mPreparedListener);
		setOnErrorListener(mErrorListener);
	}

	public void start(){
		super.start();
	}
	
	public void pause() {
		if (super.isPlaying()) {
			super.pause();
		}
	}
	
	//Listeners
	private MediaPlayer.OnBufferingUpdateListener mBufferingUpdateListener = new MediaPlayer.OnBufferingUpdateListener() {
		public void onBufferingUpdate(MediaPlayer mp, int percent) {
		}
	};
	
	MediaPlayer.OnVideoSizeChangedListener mSizeChangedListener = new MediaPlayer.OnVideoSizeChangedListener() {
		public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
			mVideoWidth = mp.getVideoWidth();
			mVideoHeight = mp.getVideoHeight();
			if (mVideoWidth != 0 && mVideoHeight != 0) {
				getHolder().setFixedSize(mVideoWidth, mVideoHeight);
			}
		}
	};
	
	private MediaPlayer.OnSeekCompleteListener mSeekCompleteListener = new MediaPlayer.OnSeekCompleteListener() {
		@Override
		public void onSeekComplete(MediaPlayer mp) {
		}
	};
	
	private MediaPlayer.OnPreparedListener mPreparedListener = new MediaPlayer.OnPreparedListener() {
		
		public void onPrepared(MediaPlayer mp) {
			Log.d(Util.TAG, "onPrepared");

	    	mp.setOnVideoSizeChangedListener(mSizeChangedListener);
	    	
	    	mp.setOnBufferingUpdateListener(mBufferingUpdateListener); 
	    	
	    	mp.setOnSeekCompleteListener(mSeekCompleteListener);
	    	
	    	mp.setOnCompletionListener(mCompletionListener);
	    	    	
	    	mp.setOnErrorListener(mErrorListener);
	    	
	    	if (progressDialog.isShowing())
	    		progressDialog.dismiss();
	    	
	    	start();
	}
		
	};
	
	
	private MediaPlayer.OnCompletionListener mCompletionListener = new MediaPlayer.OnCompletionListener() {
		public void onCompletion(MediaPlayer mp) {
			if (progressDialog.isShowing())
	    		progressDialog.hide();
		}
	};

	private MediaPlayer.OnErrorListener mErrorListener = new MediaPlayer.OnErrorListener() {
		public boolean onError(MediaPlayer mp, int framework_err, int impl_err) {
			if (progressDialog.isShowing())
				progressDialog.hide();
	    	return false;
		}
	};
	
	private MediaController mMediaController = new MediaController(getContext());

}
