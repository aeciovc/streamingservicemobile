package br.org.cesar.streamingservicemobile;

import android.app.ActionBar;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import br.org.cesar.streamingservicemobile.anymote.AnymoteCommunication;
import br.org.cesar.streamingservicemobile.anymote.ConnectionStatus;
import br.org.cesar.streamingservicemobile.model.Video;
import br.org.cesar.streamingservicemobile.streaming.VideoViewTV;

public class VideoViewActivity extends Activity {

	private Video mVideo;
	private Handler handlerBar;
	private RunnableBar runnableBar;
	
	private VideoViewTV videoViewTV;
	
	private AnymoteCommunication anymoteCommunication;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_video_play);

		handlerBar = new Handler();
		runnableBar = new RunnableBar();
		
		showOrHideActionBar(3000, false);

		Bundle bundle = getIntent().getExtras();

		mVideo = (Video) bundle.getSerializable(Video.VIDEO_PARAM);

		setTitle(mVideo.getTitle());

		videoViewTV = (VideoViewTV) findViewById(R.id.videoView);
		videoViewTV.setOnTouchListener(mOnTouchListener);
		videoViewTV.setVideo(mVideo);
		videoViewTV.start();

	}

	private OnTouchListener mOnTouchListener = new OnTouchListener() {
		@Override
		public boolean onTouch(View v, MotionEvent event) {

			boolean isShowing = getActionBar().isShowing();

			if (!isShowing) {
				VideoViewActivity.this.showOrHideActionBar(0, false);
				VideoViewActivity.this.showOrHideActionBar(3000, false);
			} else {
				//Hide action bar e remove showOrHideActionBar scheduled
				VideoViewActivity.this.showOrHideActionBar(0, true);
			}
			
			return false;
		}
	};

	private void showOrHideActionBar(final long delay, final boolean removeCallbacks) {

		if (removeCallbacks){
			handlerBar.removeCallbacks(runnableBar);
		}
		handlerBar.postDelayed(runnableBar, delay);

	}

	class RunnableBar implements Runnable {

		@Override
		public void run() {

			ActionBar actionBar = getActionBar();

			if (actionBar.isShowing())
				actionBar.hide();
			else
				actionBar.show();
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater menuInflater = getMenuInflater();
		menuInflater.inflate(R.menu.activity_video_view, menu);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {

		switch (item.getItemId()) {
		case R.id.menu_settings:
			startActivity(new Intent(this, SettingsActivity.class));
			break;
		case R.id.menu_show_on_tv:
			
			anymoteCommunication = AnymoteCommunication.getInstance(VideoViewActivity.this);
			anymoteCommunication
					.startCommunication(new AnymoteCommunication.OnAnymoteConnectionListener() {

						@Override
						public void onConnection(
								ConnectionStatus mConnectionStatus) {

							final Intent intentVideoPlay = new Intent(
									Intent.ACTION_VIEW);
							intentVideoPlay
									.setComponent(new ComponentName(
											"br.org.cesar.streamingservicetv",
											"br.org.cesar.streamingservicetv.VideoViewActivity"));
							intentVideoPlay.putExtra(Video.VIDEO_URL,
									mVideo.getAddress());
							intentVideoPlay.putExtra(Video.VIDEO_TITLE,
									mVideo.getTitle());
							intentVideoPlay.putExtra(Video.VIDEO_POSITION,
									videoViewTV.getCurrentPosition());

							anymoteCommunication.sendIntent(intentVideoPlay);
							
							videoViewTV.stopPlayback();
							
							VideoViewActivity.this.finish();
						}
					});
			
			
			break;
		}
		return super.onMenuItemSelected(featureId, item);
	}
}
