package br.org.cesar.streamingservicemobile;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import br.org.cesar.streamingservicemobile.util.Preferences;

public class SettingsActivity extends Activity {

	private EditText edtAddress;
	private Preferences preferences;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);
		
		edtAddress = (EditText) findViewById(R.id.edtAddress);
		
		preferences = new Preferences(this);
		
		edtAddress.setText(preferences.getValue(Preferences.KEY_ADDRESS, "http://"));
		 
	}
	
	public void btnSave(View view){
		preferences.update(Preferences.KEY_ADDRESS, edtAddress.getText().toString());
		Toast.makeText(this, "Address saved!", Toast.LENGTH_LONG).show();
	}
	
}
