package br.org.cesar.streamingservicemobile.util;

import android.graphics.Typeface;

public class Util {

	public static final String TAG = "StreamingService";
	
	public static final String SERVER_URL = "http://172.28.147.53";
	public static final String FILE_NAME_JSON = "/all.json";
	
	public static final String TYPEFACE_IMPACT_ASSET = "impact.ttf";
	public static final String TYPEFACE_ARIAL_ASSET = "arial.ttf";
	public static final String TYPEFACE_LHANDW = "LHANDW.TTF";
	
	/**
	 * 
	 * Used to change the typeface in text views with the method
	 * setTypeFace. 
	 * @param typefaceAsset - path to the typeface in the assets folder.
	 * @return typeface font
	 */
	
	public static Typeface getTypeface(String typefaceAsset) {
		return Typeface.createFromAsset(UtilContext.getContextApp().getAssets(),
				typefaceAsset);
	}
	
	
	
}
