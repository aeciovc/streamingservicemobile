package br.org.cesar.streamingservicemobile.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class AndroidUtils {
	protected static final String TAG = "AndroidUtils";
	
	/** Checks for an existing network connectivity
	 * 
	 * @param context
	 *            The {@link Context} which is needed to tap
	 *            {@link Context#CONNECTIVITY_SERVICE}
	 * @return True if network connection is available
	 */
	public static boolean isNetworkAvailable(Context context) {
		ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity == null) {
			return false;
		} else {
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null) {
				for (int i = 0; i < info.length; i++) {
					if (info[i].getState() == NetworkInfo.State.CONNECTED) {
						return true;
					}
				}
			}
		}
		return false;
	}
	
}
