package br.org.cesar.streamingservicemobile;

import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.Toast;
import br.org.cesar.streamingservicemobile.anymote.AnymoteCommunication;
import br.org.cesar.streamingservicemobile.anymote.ConnectionStatus;
import br.org.cesar.streamingservicemobile.model.Video;
import br.org.cesar.streamingservicemobile.model.VideoController;
import br.org.cesar.streamingservicemobile.util.AndroidUtils;
import br.org.cesar.streamingservicemobile.util.Preferences;
import br.org.cesar.streamingservicemobile.util.Util;

public class VideosActivity extends Activity {

	private GridView gridView;
	private VideoAdapter adapter;
	private AsyncTask<Void, Void, Void> videosTask;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		gridView = (GridView) findViewById(R.id.gridview);
		
		getVideosFromServer();
		
	}

	private void getVideosFromServer() {
		if (AndroidUtils.isNetworkAvailable(this)) {
			//check if any previous task is running, if so then cancel it
	        //it can be cancelled if it is not in FINISHED state
	        if (videosTask != null && videosTask.getStatus() != AsyncTask.Status.FINISHED) {
	        	videosTask.cancel(true);
	        }
	        //every time create new object, as AsynTask will only be executed one time.
	        videosTask = new GetVideosTask(); 
	        videosTask.execute();
		} else {
			Toast.makeText(VideosActivity.this, getString(R.string.error_no_network), Toast.LENGTH_SHORT).show();
		}
	}
	
	class GetVideosTask extends AsyncTask<Void, Void, Void> {

		ProgressDialog dialog = new ProgressDialog(VideosActivity.this);
		List<Video> videos;

		protected void onPreExecute() {
			dialog.setMessage("Please wait...");
			dialog.setIndeterminate(true);
			dialog.setOnDismissListener(new OnDismissListener() {
				
				@Override
				public void onDismiss(DialogInterface dialog) {
					if (videosTask != null && videosTask.getStatus() != AsyncTask.Status.FINISHED) {
			        	videosTask.cancel(true);
			        }
				}
			});
			
			dialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
				
				Preferences preferences = new Preferences(VideosActivity.this);
				String url = preferences.getValue(Preferences.KEY_ADDRESS, Util.SERVER_URL);
			
				videos = VideoController.getVideos(url);
				// http://crk_br_am_ios-i.akamaihd.net/i/1/w/z6/jsvlf_H264_IOS_,100BR,200BR,350BR,500BR,650BR,800BR,900BR,.mp4.csmil/master.m3u8?__b__=400&__a__=off
	
				if (videos != null) {
					adapter = new VideoAdapter(VideosActivity.this, videos);
				}
			} catch (Exception e) {
				if (!isCancelled()) {
					dialog.dismiss();
					Toast.makeText(VideosActivity.this, "Error on connect to Server: " + e.getMessage(), Toast.LENGTH_SHORT).show();
				}
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(Void unused) {
			
			if (adapter != null) {
				gridView.setAdapter(adapter);
				gridView.setOnItemClickListener(new OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> parent, View v,
							final int position, long id) {
						
						Intent intent = new Intent(VideosActivity.this, VideoViewActivity.class);
						intent.putExtra(Video.VIDEO_PARAM, videos.get(position));
						startActivity(intent);
					}
				});
			}

			dialog.dismiss();
		}
		
		@Override
		protected void onCancelled() {
			Log.i("VideosActivity", "Task Cancelled");
			super.onCancelled();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater menuInflater = getMenuInflater();
		menuInflater.inflate(R.menu.activity_main, menu);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {

		switch (item.getItemId()) {
		case R.id.menu_settings:
			startActivity(new Intent(VideosActivity.this,
					SettingsActivity.class));
			break;
		case R.id.menu_reconnect:
			getVideosFromServer();
			break;
		}
		return super.onMenuItemSelected(featureId, item);
	}

	@Override
	protected void onDestroy() {
        if (videosTask != null && videosTask.getStatus() != AsyncTask.Status.FINISHED) {
        	videosTask.cancel(true);
        }
		super.onDestroy();
	}
}
